# -*- coding: utf-8 -*-
"""
Created on Fri Jul  7 14:50:00 2022

@author: Alexander Böhmländer
"""

import warnings
from typing import TypeVar, Optional
import numpy.typing as npt

PandasDataFrame = TypeVar('pandas.core.frame.DataFrame')
warnings.filterwarnings("ignore", category=RuntimeWarning)


def add_asym(x0s: npt.ArrayLike, siglos: npt.ArrayLike, sighis: npt.ArrayLike,
             order: int = 2, nmax: int = 100, tol: float = 1e-6) -> npt.ArrayLike:
    """
    Purpose
    -------
    Calculate mean of two or more numbers with asymmetric uncertainties.

    Attribution
    -----------
    If you use this function for scientific work leading to a publications,
    please cite
      Laursen et al. (2019, A&A, 627, 84),
    where the code is described, tested, and applied.

    Description
    -----------
    Each number is characterized by three numbers, viz. its central value x0,
    and its standard deviations siglo and sighi toward negative and positive
    values, respectively.

    That is, the numbers would be written as
      X = x0_{-siglo_x}^{+sighi_x}
      Y = y0_{-siglo_y}^{+sighi_y}
      Z = z0_{-siglo_z}^{+sighi_z}
      etc.

    The standard approach for this problem is to add the central values
    normally, and to add the lower and upper uncertainties separately in
    quadrature, i.e.
      X + Y = {x0+y0}_{-siglo_tot}^{+sighi_tot},
    where
      siglo_tot^2 = siglo_x^2 + siglo_y^2;
      sighi_tot^2 = sighi_x^2 + sighi_y^2.
    This method has no statistical foundation, and is wrong.

    There is no one right method to solve this problem (as infinitely many PDFs
    could be described by the same three numbers), but one method that seems to
    give acceptable results for many different distributions is described in
    Barlow, R. (2003), http://arxiv.org/abs/physics/0306138v1.
    In short, the distributions are transformed --- linearly or quadratically
    --- to "proper" Gaussians, for which the normal, linear addition is valid.
    The total mean, variance, and skewness can thus be found, and these values
    are then used to transform back to get the three numbers for the final
    distribution.
    Whereas the "forward" transformation can be done analytically, the
    "backward" transformation must be done numerically by iteration.

    Parameters
    ----------
    x0s:    array of central values.
    siglos: array of lower uncertainties.
    sighis: array of upper uncertainties.
    order:  order of the transformation (1 for linear, 2 for quadratic, and
            0 for the standard, but wrong, method of adding upper and
            lower uncertainties separately in quadrature.
    nmax:   Maximum number of iterations (usually <10 are needed).
    tol:    Tolerance for the accepted result.

    Returns
    -------
    numpy array containing the three numbers x0_tot, siglo_tot, and sighi_tot
    that best describe the PDF of the sum.

    Example
    -------
    Do the following addition, using a linear transformation:
    5_{-2}^{+1}  +  3_{-3}^{+1}  +  4_{-3}^{+2}.
    >>> x0 = [5,3,4]
    >>> s1 = [2,3,3]
    >>> s2 = [1,1,2]
    >>> add_asym(x0,s1,s2,order=1)
    array([ 10.92030132,   4.23748786,   2.94389109])

    That is, the sum would be written as 10.9_{-4.2}^{+2.9}.

    Do the same, using a quadratic transformation:
    >>> add_asym(x0,s1,s2,order=2)
    array([ 10.65168907,   4.47929965,   3.1759215 ])

    Notice that the result hinges somewhat on the chosen order, but that both
    find a lower central value than the "standard", but wrong, result of
    12_{-4.69}^{+2.45}. The reason is that in this case, all three addends have
    PDFs that are skewed toward lower values. Note also that both methods give
    more symmetric errors, in accord with the Central Limit Theorem. In
    contrast, the asymmetry of addends whose skewness are of the same sign will
    never decrease.
    """

    import numpy as np

    sqrt2pi = np.sqrt(2.0 * np.pi)

    x0s = np.asarray(x0s)
    siglos = np.asarray(siglos)
    sighis = np.asarray(sighis)

    # ------------------   First, calculate total moments   -----------------
    mu, V, gamma = 0., 0., 0.
    if order == 1:  # Cumul. mean,var,skew
        sig = (sighis+siglos) / 2.  # "The mean" }_eq. 1
        alpha = (sighis-siglos) / 2.  # "The diff."
        mu = np.sum(x0s + (sighis-siglos) / sqrt2pi)  # Biased mean
        V = np.sum(sig**2 + alpha**2 * (1-2 / np.pi))  # Variance
        t1 = sighis**3 - siglos**3
        t2 = (sighis-siglos) * (sighis**2+siglos**2)  # > 3 terms for gamma
        t3 = (sighis-siglos)**3
        gamma = np.sum(2.*t1-1.5*t2+t3 / np.pi) / sqrt2pi  # Skewness
    else:  # Same for order = 2
        sig = (sighis+siglos) / 2.  # "The mean" }_eq. 1
        alpha = (sighis-siglos) / 2.  # "The diff."
        mu = np.sum(x0s + alpha)  # Biased mean
        V = np.sum(sig**2 + 2*alpha**2)  # Variance
        gamma = np.sum(6*sig**2 * alpha + 8*alpha**3)  # Skewness

    # -----------------   Check if iteration is necessary  ------------------
    check = abs(gamma/mu**3) if mu != 0. else gamma  # Avoid NaN for mu = 0
    if check < 1e-12:  # If errors are too close to
        x0 = mu  # being symmetric, the method
        siglo = np.sqrt(V)  # below gives an exception,
        sighi = siglo  # so calculate normally

    else:
        # -----------  Use moments to find resulting distribution  --------------
        n = 0  # Counter for # of iterations
        if order == 1:
            D = 0.  # Iterator; eq. to sighi-siglo
            while True:
                n += 1  # Update counter
                S = 2*V + D**2/np.pi  # S is equal to siglo^2+sighi^2
                Dold = D
                D = 2/(3.*S) * (sqrt2pi*gamma - D**3 * (1/np.pi-1))  # Iterate!
                if Dold != 0.:
                    if abs(D/Dold-1.) < tol:
                        break
                assert n < nmax, 'Too many iterations'
            S = 2*V + D**2/np.pi  # Final update of S
            x0 = mu - D/sqrt2pi  # Biased mean
            alpha = D / 2.  # "The difference" }_ eq. 1
            sig = np.sqrt(V - alpha**2*(1-2/np.pi))  # "The mean"
            siglo = sig - alpha  # Lower error
            sighi = sig + alpha  # Upper error
        elif order == 2:
            alpha = 0.  # Iterator; equal to (hi-lo)/2
            while True:
                n += 1  # Update counter
                aold = alpha
                alpha = gamma / (6*V - 4*alpha**2)  # Iterate!
                if aold != 0.:  # |
                    if abs(alpha/aold-1.) < tol:
                        break  # Accept if close enough
                assert n < nmax, 'Too many iterations'
            sig = np.sqrt(V - 2*alpha**2)  # "The mean" (eq 1)
            x0 = mu - alpha
            siglo = sig - alpha  # >Final values
            sighi = sig + alpha

    return np.array([x0, siglo, sighi]) / len(x0s)


def raw_data_func(exp_id: int, plot: bool = False, single_outlier: bool = True,
                  std: bool = True, heating_period: bool = True) -> tuple:
    """
    Function that returns the raw temperature data. Option to see which parts are removed
    due to single_outliers, standard deviation as well as the heating period.

    Parameters
    ----------
    exp_id : int
        Experiment id of a given experiment.
    plot : bool, optional
        If plots should be created. Not that in the case of plot=True, the return is different.
        The default is False.
    single_outlier : bool, optional
        If a single temprature sensor should be removed or not. The default is True.
    std : bool, optional
        If data above a certain standard deviation should be removed or not. The default is True.
    heating_period : bool, optional
        If heating period should be removed or not. The default is True.

    Returns
    -------
    tuple

    If plot is False:
        Two pandas DataFrames are returned as a tuple. The first "df" is the temperature sensor
        data after removal via the various filters, whereas the second one "df_raw" returns the raw
        temperature sensor data.
    If plot is True:
        The same is returned as before, but in addition the two figures and their axes are
        returned. This way the figures and axes can still be used and manipulated.

    """
    import pandas as pd
    from .INSEKT_selection import select_experiments, check_credentials
    import sqlalchemy as db

    username, password = check_credentials()
    insekt = db.create_engine(f'mysql+mysqlconnector://{username}:{password}'
                              '@141.52.172.3:3306/insekt')

    data = select_experiments(exp_id=exp_id)

    x = data['insekt_id'][data['exp_id'] == exp_id].to_numpy()[0]

    prep_time = data['prep_time'].dt.date.to_numpy()[0]

    calibration_data = pd.read_sql_query(f"SELECT * FROM insekt.temp_calib_0{x} "
                                         f"WHERE `datetime` < '{prep_time}'", insekt).iloc[-1, :]

    t_start = data['t_start'][data['exp_id'] == exp_id].to_numpy()[0]
    t_stop = data['t_stop'][data['exp_id'] == exp_id].to_numpy()[0]
    query = (f"SELECT * FROM insekt.rawdata_0{x} WHERE datetime BETWEEN "
             f"'{t_start}' AND '{t_stop}'")
    df = pd.read_sql_query(query, insekt)

    for i, column in enumerate(df.loc[:, 'T1':'T8'].columns):
        df[column] = (calibration_data[f'a{i+1}'] * df[column]
                      + calibration_data[f'b{i+1}'])

    df_raw = df.copy(deep=True)

    col = df.loc[:, 'T1':'T8']

    # Remove all Tsensors if they are further than 0.5 K from the mean.
    mean_list = [abs(col.mean(axis=0)[index] - col.mean(axis=0).mean())
                 for index in col.mean(axis=0).index]
    outlier_lst = [idx for (idx, _) in enumerate(mean_list) if _ > 0.5]

    if single_outlier:
        if len(outlier_lst) > 0:
            for outlier in outlier_lst:
                col.drop(f'T{outlier+1}', axis=1, inplace=True)
                print(f"Sensor T{outlier+1} dropped for mean.")

    # Quick work around because Experiment failed to monitor other sensors.
    if exp_id == 4046:
        df['Mean_raw'] = df.loc[:, 'T1']
        df['Mean'] = df.loc[:, 'T1']
        df['std'] = 0.1
    else:
        df['Mean_raw'] = df.loc[:, 'T1':'T8'].mean(axis=1)
        df['Mean'] = col.mean(axis=1)
        df['std'] = col.std(axis=1, ddof=0)
        df_raw['Mean'] = df_raw.loc[:, 'T1':'T8'].mean(axis=1)
        df_raw['std'] = df_raw.loc[:, 'T1':'T8'].std(axis=1, ddof=0)

    # remove all values that have a temperature smaller than 200 K, important for old experiments
    df = df.loc[df['Mean'] > 200, :]

    if std:
        # remove values where standard deviation is bigger than 0.5 K
        start_length = len(df.index)
        df = df.loc[~(df.loc[:, 'std'] > 0.5), :]
        print(f'Removed {start_length - len(df.index)} rows due to '
              'T standard deviation larger than 0.5 K.')

    if heating_period:
        # remove heating up period from data
        df = df.loc[:df.loc[df['Mean'] == df.loc[:, 'Mean'].min()].index[-1], :]

    if plot:
        import matplotlib.pyplot as plt
        plt.rcParams.update(plt.rcParamsDefault)
        plt.rcParams.update({'font.size': 18,
                             'interactive': False})

        fig1, (ax1, ax1_2) = plt.subplots(2, 1, figsize=(16, 9), sharex=True,
                                          constrained_layout=True)
        for line in (set(df_raw.index) - set(df.index)):
            ax1_2.axvline(line, color='k', alpha=0.5, zorder=0)
        for i in range(1, 8+1):
            if len(outlier_lst) > 0:
                if i in [_ + 1 for _ in outlier_lst]:
                    ax1.plot(df.index, df[f'T{i}'] - df['Mean'], zorder=1, alpha=0,
                             label=f'T{i} (removed)')
                    df_copy = df.copy(deep=True)
                    df_copy.drop([f'T{_+1}' for _ in outlier_lst], axis=1, inplace=True)
                    ax1.set_ylim((df_copy.loc[:, df_copy.columns.str.startswith('T')].sub(
                        df['Mean'], axis=0)).max().max() * 1.05,
                        (df_copy.loc[:, df_copy.columns.str.startswith('T')].sub(
                            df['Mean'], axis=0)).min().min() * 1.05)
                    med = (df_raw[f'T{i}'] - df_raw['Mean']).mean()
                    ax1_2.plot(df_raw.index, df_raw[f'T{i}'] - df_raw['Mean'], zorder=1,
                               label=f'T{i}: {med:.3f}')
                else:
                    ax1.plot(df.index, df[f'T{i}'] - df['Mean'], zorder=1, label=f'T{i}')
                    med = (df_raw[f'T{i}'] - df_raw['Mean']).mean()
                    ax1_2.plot(df_raw.index, df_raw[f'T{i}'] - df_raw['Mean'], zorder=1,
                               label=f"T{i}: {med:.3f}")
            else:
                ax1.plot(df.index, df[f'T{i}'] - df['Mean'], zorder=1, label=f'T{i}')
                med = (df_raw[f'T{i}'] - df_raw['Mean']).mean()
                ax1_2.plot(df_raw.index, df_raw[f'T{i}'] - df_raw['Mean'], zorder=1,
                           label=f"T{i}: {med:.3f}")
        ax1.set_title(f'single outlier removed: {single_outlier}, '
                      f'std (> 0.5 K) removed: {std}, '
                      f'heating period removed: {heating_period}')
        ax_twin = ax1.twinx()
        ax_twin.plot(df['std'], color='k', label=r'$T_\mathrm{std}$')
        ax_twin.set_ylabel(r'$T_\mathrm{std}$ / K')

        ax_twin2 = ax1_2.twinx()
        ax_twin2.plot(df_raw['std'], color='k', label=r'$T_\mathrm{std}$')
        ax_twin2.set_ylabel(r'$T_\mathrm{std}$ / K')

        ax1.axhline(0)
        ax1_2.axhline(0)
        ax1.set_ylabel(r'T$i$ - $T_\mathrm{mean}$ / K')
        ax1_2.set_xlabel('index / -')
        ax1_2.set_ylabel(r'T$i$ - $T_\mathrm{mean}$ / K')
        lines, labels = ax1.get_legend_handles_labels()
        lines2, labels2 = ax_twin.get_legend_handles_labels()
        ax_twin.legend(lines + lines2, labels + labels2,
                       title=f'corrected, length = {len(df.index)}', loc='upper left')

        lines, labels = ax1_2.get_legend_handles_labels()
        lines2, labels2 = ax_twin2.get_legend_handles_labels()
        ax_twin2.legend(lines + lines2, labels + labels2,
                        title=f'uncorrected, length = {len(df_raw.index)}', loc='upper left')

        fig2, (ax2, ax2_2) = plt.subplots(2, 1, figsize=(16, 9), sharex=True,
                                          constrained_layout=True)
        for line in (set(df_raw.index) - set(df.index)):
            ax2_2.axvline(line, color='k', alpha=0.5, zorder=0)

        ax2.set_title(f'single outlier removed: {single_outlier},'
                      f'std (> 0.5 K) removed: {std},'
                      f'heating period removed: {heating_period}')
        ax2.plot(df['Mean'], zorder=1, label='Mean')
        ax2_2.plot(df_raw['Mean'], zorder=1, label='Mean raw')
        ax2.set_ylabel(r'$T_\mathrm{mean}$ / K')
        ax2_2.set_ylabel(r'$T_\mathrm{mean}$ / K')
        ax2_2.set_xlabel('index / -')
        ax2.legend(title=f'corrected, length = {len(df.index)}', loc='upper left')
        ax2_2.legend(title=f'raw, length = {len(df_raw.index)}', loc='upper left')
        return df, df_raw, (fig1, ax1, ax1_2), (fig2, ax2, ax2_2)

    return df, df_raw


def grey_scale_data(exp_id: int, df: Optional[PandasDataFrame] = None,
                    df_raw: Optional[PandasDataFrame] = None) -> tuple:
    """
    Function to get grey scale data and freezing index.

    Parameters
    ----------
    exp_id : int
        Experiment id of a given experiment.
    df : Optional[PandasDataFrame], optional
        DataFrame of data from raw_data_func. The default is None.
    df_raw : Optional[PandasDataFrame], optional
        ataFrame of raw data from raw_data_func. The default is None.

    Returns
    -------
    tuple
        Raw data from greyscale values as well as freezing index.

    """

    import numpy as np
    import pandas as pd
    from .INSEKT_selection import select_experiments

    data = select_experiments(exp_id=exp_id)

    if df is None or df_raw is None:
        df, df_raw = raw_data_func(exp_id=exp_id)

    raw_data = df['rawdata'].map(lambda _: np.delete(np.frombuffer(_[::-1], dtype=np.float32), -1))
    raw_data = pd.DataFrame(raw_data.to_list(), index=df.index).T

    # For certain partitions, Nanopure water is denoted as 1, changing that to 0
    # In these case the function will only return the frozen fraction!
    if 0 not in data.loc[data['exp_id'] == exp_id, 'data'].to_numpy()[0]:
        data.loc[data['exp_id'] == exp_id, 'data'] = data.loc[data['exp_id'] == exp_id, 'data'] - 1

    number_wells = dict(zip(*np.unique(data['data'][data['exp_id'] == exp_id].to_numpy()[0],
                                       return_counts=True)))

    raw_data.insert(loc=0, column='partition',
                    value=data['data'][data['exp_id'] == exp_id].to_numpy()[0])
    grey_scale = raw_data

    freeze_index = [grey_scale.iloc[:, 1:].diff(axis=1).iloc[i, :].idxmin()
                    if grey_scale.iloc[:, 1:].diff(axis=1).iloc[i, :].min() < -30
                    else np.nan
                    for i in range(192)
                    ]

    def cond(y, freeze_index, i):
        if y <= freeze_index[i]:
            return 1
        elif np.isnan(freeze_index[i]):
            return 1
        else:
            return 0

    _lst = [pd.Series([cond(y, freeze_index, i) for y in raw_data.columns[1:]])
            for i in range(len(freeze_index))]
    liquid_fraction = pd.concat(_lst, axis=1)
    liquid_fraction.index = df.index
    return grey_scale, number_wells, freeze_index, df


def single_experiment(exp_id: int, T_step: float | None = 0.5,
                      exp_id_blank: bool = False,
                      q_value: int = 8) -> dict[str, PandasDataFrame]:
    """
    Creates analysis data from raw_data of a given experiment in INSEKT.
    The data is treated and saved in various forms as pkl files.

    Parameters
    ----------
    exp_id : int
        Experiment id of a given experiment.
    T_step : Union[float, None], optional
        The temperature grid, where data points lie. The grid is on the Kelvin scale.
        If None is given, the data will be spread as events, i.e. whenever a well freezes,
        one data point is generated. The default is 0.5.
    exp_id_blank : bool, optional
        Experiment id of a given handling blank. Not recommended, better to check
        frozen fraction of blank filters and decide whether they are "good" or "bad".
        The default is False.
    q_value : int, optional
        q_value is a measure of the asymmetry in the uncertainties calculated
        with the approximated bernoulli PDF from Agresti & Coull. The default is 8.

    Returns
    -------
    Dict[str, PandasDataFrame]
        Returns a data dictionary for the given experiment id.
        It has the following form:
            data_dct = {'data': data,
                        'data_corrected': data_corrected,
                        'data_frozen_fraction': data_ff}
        data by itself contains the data from the suspension and the dilutions. This data
            is already corrected with the q_value and is already gridded to the chosen
            T_step. In addition, this data is also restricted to only positive concentration
            values.
        data_corrected combines different suspensions and dilutions together. I.e. if you
            select a T_step of 0.5 (the default) and data points from different dilutions
            are at the same temperature, then these will be combined in a single data point
            by taking the mean.
            The uncertainties are also combined according to the formula
                specified in add_asym() function.

    """

    import numpy as np
    import pandas as pd
    from pathlib import Path
    from .INSEKT_calculations import (c_inp_np_corr, c_inp_sol, c_inp_unc)
    from .INSEKT_selection import select_experiments, check_credentials
    import sqlalchemy as db
    import copy

    pkl_path = Path(__file__).parent.joinpath('Pickles')

    username, password = check_credentials()
    insekt = db.create_engine(f'mysql+mysqlconnector://{username}:{password}'
                              '@141.52.172.3:3306/insekt')

    data = select_experiments(exp_id=exp_id)

    bulk = data['type'].to_numpy()[0] == 'bulk'

    x = data['insekt_id'][data['exp_id'] == exp_id].to_numpy()[0]

    prep_time = data['prep_time'].dt.date.to_numpy()[0]

    calibration_data = pd.read_sql_query(f"SELECT * FROM insekt.temp_calib_0{x} "
                                         f"WHERE `datetime` < '{prep_time}'", insekt).iloc[-1, :]

    t_calib = calibration_data['descr'].replace(' ', '_')

    df, df_raw = raw_data_func(exp_id=exp_id)

    grey_scale, number_wells, freeze_index, _ = grey_scale_data(exp_id=exp_id,
                                                                df=df,
                                                                df_raw=df_raw)

    def cond(y, freeze_index, i):
        if y <= freeze_index[i]:
            return 1
        elif np.isnan(freeze_index[i]):
            return 1
        else:
            return 0

    _lst = [pd.Series([cond(y, freeze_index, i) for y in grey_scale.columns[1:]])
            for i in range(len(freeze_index))]

    liquid_fraction = pd.concat(_lst, axis=1)
    liquid_fraction.index = df.index

    dilution_scales = list(map(int,
                               data['dilname'][data['exp_id'] == exp_id].to_numpy()[0].split(',')))

    query_timeseries = f"SELECT * FROM insekt.larissa WHERE exp_id = '{exp_id}'"

    data_timeseries = pd.read_sql_query(query_timeseries, insekt)

    liquid_fraction = liquid_fraction.T
    liquid_fraction.insert(loc=0, column='partition', value=grey_scale['partition'])
    percentage_dilutions = {}
    for i in range(max(number_wells) + 1):
        percentage_dilutions[f'Dilution {i}'] = (
            liquid_fraction[liquid_fraction['partition'] == i].sum()
            / len(liquid_fraction[liquid_fraction['partition'] == i]))[1:]

    # If some temperatures are lower than 200 K, they are estimated as the average
    # of the surrounding temperatures and overwritten.
    # If these temperatures occur at the first or the last element,
    # it is instead overwritten as a np.nan.
    # TODO: add logging interface, extrapolate instead of np.nan?
    wrong_temp_index = df[~(df['Mean'] > 200)].index.to_list()
    for idx in wrong_temp_index:
        try:
            df.loc[df.index[idx], 'Mean'] = (df.loc[df.index[idx-1], 'Mean']
                                             + df.loc[df.index[idx+1], 'Mean']) / 2
        except IndexError:
            df.loc[df.index[idx], 'Mean'] = np.nan

    V_sol = data['solvent_vol_(ml)'].to_numpy()[0]

    if bulk:
        mass_query = ("SELECT * from insekt.exp_all WHERE "
                      f"exp_id = '{exp_id}'")

        mass = pd.read_sql_query(mass_query, insekt)
        V_air = mass['bulkmass_(mg)'].to_numpy()[0]
        sample_time = 1
    else:
        flow_query = (f"SELECT * from insekt.filter WHERE "
                      f"filter_id = '{data['type_id'][data['exp_id'] == exp_id].to_numpy()[0]}'")

        flow = pd.read_sql_query(flow_query, insekt)
        V_air = flow['flow (l/min)'].to_numpy()[0]
        sample_time = ((data_timeseries.loc[0, 'stoptime']
                        - data_timeseries.loc[0, 'starttime'])
                       .total_seconds() / 60)

    if isinstance(T_step, (float, int)):
        temp_list = np.arange(round(df['Mean'].min() / T_step) * T_step,
                              round((df['Mean'].max() + T_step) / T_step) * T_step, T_step)
        closest_temps = []
        for value in temp_list:
            closest_temps.append(min(df['Mean'], key=lambda x: abs(x - value)))
        closest_temps_copy = closest_temps[:]
        for temp1, temp2 in zip(temp_list, closest_temps_copy):
            if abs(temp1 - temp2) > df['std'].mean():
                closest_temps.remove(temp2)
        temp_index = df[df['Mean'].isin(closest_temps)].index

    ff_dict = {}

    percentage_dilutions_copy = copy.deepcopy(percentage_dilutions)
    for i, name in enumerate(percentage_dilutions_copy.keys()):
        if isinstance(T_step, type(None)):
            percentage_dilutions_copy[name].drop_duplicates(keep='first', inplace=True)
            index = percentage_dilutions_copy[name].index
        elif isinstance(T_step, (float, int)):
            index = temp_index

        frozen_fraction = percentage_dilutions[name].loc[index].map(lambda x: 1 - x)

        if i == 0:
            uncertainties = c_inp_unc(frozen_fraction,
                                      number_wells[i], 1)
        else:
            uncertainties = c_inp_unc(frozen_fraction,
                                      number_wells[i], dilution_scales[i-1])
        low_unc = pd.Series(uncertainties['lower_limit_IN'])
        upp_unc = pd.Series(uncertainties['upper_limit_IN'])
        ff_low_unc = frozen_fraction - pd.Series(uncertainties['lower_limit_wells'])
        ff_upp_unc = pd.Series(uncertainties['upper_limit_wells']) - frozen_fraction
        # converting all indexes to dtype int64, explicit is better than implicit
        frozen_fraction.index.astype(dtype='int64')
        ff_low_unc.index.astype(dtype='int64')
        ff_upp_unc.index.astype(dtype='int64')
        data_ff = pd.concat([df['Mean'].loc[df['Mean'].index.intersection(index)],
                             df['std'].loc[df['Mean'].index.intersection(index)],
                             frozen_fraction, ff_low_unc, ff_upp_unc],
                            axis=1)
        data_ff.columns = ['T_mean / K', 'T_std / K',
                           'frozen_fraction / -',
                           'frozen_fraction_unc_minus / -',
                           'frozen_fraction_unc_plus / -']
        ff_dict[name] = data_ff

    data_dict = {}
    for i, name in enumerate(percentage_dilutions.keys()):
        if isinstance(T_step, type(None)):
            if name != 'Dilution 0':
                percentage_dilutions[name].drop_duplicates(keep='first', inplace=True)
            index = percentage_dilutions[name].index
        elif isinstance(T_step, (float, int)):
            index = temp_index

        if name != 'Dilution 0':
            c_INP = c_inp_np_corr(V_sol, V_air, sample_time, dilution_scales[i-1],
                                  percentage_dilutions['Dilution 0'][index],
                                  percentage_dilutions[name][index])

            c_INP_sol = c_inp_sol(dilution_scales[i-1], percentage_dilutions[name][index], 1)
            frozen_fraction = percentage_dilutions[name][index].map(lambda x: 1 - x)
            uncertainties = c_inp_unc(frozen_fraction,
                                      number_wells[i-1], dilution_scales[i-1])
            low_unc = pd.Series(uncertainties['lower_limit_IN'])
            upp_unc = pd.Series(uncertainties['upper_limit_IN'])
            minus_unc = c_INP - low_unc * c_INP / c_INP_sol
            plus_unc = upp_unc * c_INP / c_INP_sol - c_INP
            ff_low_unc = frozen_fraction - pd.Series(uncertainties['lower_limit_wells'])
            ff_upp_unc = pd.Series(uncertainties['upper_limit_wells']) - frozen_fraction
            limit_of_detection = pd.Series(
                [1 / (V_air * sample_time) * (V_sol * 1e-3)
                 / (50e-6 * number_wells[i-1]) * dilution_scales[i-1]
                 ] * len(c_INP),
                index=df['Mean'].loc[df['Mean'].index.intersection(index)].index)
            # converting all indexes to dtype int64, explicit is better than implicit
            c_INP.index.astype(dtype='int64')
            c_INP_sol.index.astype(dtype='int64')
            minus_unc.index.astype(dtype='int64')
            plus_unc.index.astype(dtype='int64')
            frozen_fraction.index.astype(dtype='int64')
            ff_low_unc.index.astype(dtype='int64')
            ff_upp_unc.index.astype(dtype='int64')
            limit_of_detection.index.astype(dtype='int64')
            data = pd.concat([df['Mean'].loc[df['Mean'].index.intersection(index)],
                              df['std'].loc[df['Mean'].index.intersection(index)],
                              c_INP, c_INP_sol, minus_unc, plus_unc,
                              frozen_fraction, ff_low_unc, ff_upp_unc,
                              limit_of_detection],
                             axis=1)
            if bulk:
                data.columns = ['T_mean / K', 'T_std / K', 'c_INP / 1/mg', 'c_INP_sol / 1/l_sol',
                                'c_INP_unc_minus / 1/mg', 'c_INP_unc_plus / 1/mg',
                                'frozen_fraction / -',
                                'frozen_fraction_unc_minus / -', 'frozen_fraction_unc_plus / -',
                                'limit_of_detection / 1/mg']
            else:
                data.columns = ['T_mean / K', 'T_std / K', 'c_INP / 1/l', 'c_INP_sol / 1/l_sol',
                                'c_INP_unc_minus / 1/l', 'c_INP_unc_plus / 1/l',
                                'frozen_fraction / -',
                                'frozen_fraction_unc_minus / -', 'frozen_fraction_unc_plus / -',
                                'limit_of_detection / 1/l']
            data_dict[name] = data

    data_ff = pd.concat(ff_dict)
    data_ff = data_ff.loc[~(data_ff.loc[(slice(None), slice(None)), 'frozen_fraction / -'] == 0.0)]

    # Test
    data_ff = data_ff[(data_ff['frozen_fraction_unc_plus / -']
                       / data_ff['frozen_fraction_unc_minus / -']).between(0, q_value,
                                                                           inclusive='both')]

    if len(percentage_dilutions.keys()) == 1 and next(iter(percentage_dilutions)) == 'Dilution 0':
        pkl_path.joinpath(f"{exp_id}").mkdir(parents=True, exist_ok=True)
        if not T_step:
            T_step = 'event'
        data_ff.to_pickle(pkl_path.joinpath(
            f'{exp_id}/INSEKT_{exp_id}_{T_step}'
            f'_frozen_fraction_T{t_calib}.pkl'))
        print(f'Created ./Pickles/{exp_id}/INSEKT_{exp_id}'
              f'_{T_step}_frozen_fraction_T{t_calib}.pkl')
        data_dct = {'data_frozen_fraction': data_ff}
        return data_dct

    data = pd.concat(data_dict)

    # Implementation of q_value as shown in Master's thesis of Julia Kaufmann (p. 19)
    if bulk:
        data = data[data['c_INP / 1/mg'] > 0]
        data = data[(data['c_INP_unc_plus / 1/mg']
                     / data['c_INP_unc_minus / 1/mg']).between(1/q_value, q_value,
                                                               inclusive='both')]
    else:
        data = data[data['c_INP / 1/l'] > 0]
        data = data[(data['c_INP_unc_plus / 1/l']
                     / data['c_INP_unc_minus / 1/l']).between(1/q_value, q_value,
                                                              inclusive='both')]

    data_corrected = data.copy(deep=True)
    print(f'Exp_id: {exp_id} - uncorrected data \n', data)

    if bulk:
        data_corrected_old = data_corrected.copy(deep=True)
        df_lst = []
        for dilution in data_corrected_old.index.get_level_values(0).unique():
            maximum_index = data_corrected_old.loc[(dilution, slice(None)),
                                                   'c_INP / 1/mg'].idxmax()
            df_dilution = data_corrected_old.loc[(dilution, slice(None)), :]
            df_dilution.index = df_dilution.index.get_level_values(1)
            df_dilution = df_dilution.loc[:maximum_index[1], :]
            df_dilution.drop_duplicates(subset='T_mean / K', keep='first', inplace=True)

            df_dilution['dilution'] = [dilution for _ in range(len(df_dilution.index))]
            df_dilution = df_dilution.set_index('dilution', append=True)
            df_dilution = df_dilution.reorder_levels(['dilution', None])
            df_lst.append(df_dilution)

        try:
            data_corrected = pd.concat(df_lst, join='outer')
        except ValueError as e:
            print(f'{e}.')

        # no uncertainty propagation if events are choosen
        if T_step is None:
            duplicate_temps = []
        else:
            duplicate_temps = data_corrected.loc[
                data_corrected.duplicated(subset='T_mean / K', keep=False), 'T_mean / K'].unique()

        unc_propagation = {}
        for temperature in duplicate_temps:
            cINP_air = data_corrected.loc[
                data_corrected['T_mean / K'] == temperature,
                'c_INP / 1/mg'].to_numpy()
            cINP_air_unc_minus = data_corrected.loc[
                data_corrected['T_mean / K'] == temperature,
                'c_INP_unc_minus / 1/mg'].to_numpy()
            cINP_air_unc_plus = data_corrected.loc[
                data_corrected['T_mean / K'] == temperature,
                'c_INP_unc_plus / 1/mg'].to_numpy()

            cINP_sol = data_corrected.loc[
                data_corrected['T_mean / K'] == temperature,
                'c_INP_sol / 1/l_sol'].to_numpy()

            ff = data_corrected.loc[
                data_corrected['T_mean / K'] == temperature,
                'frozen_fraction / -'].to_numpy()
            ff_unc_minus = data_corrected.loc[
                data_corrected['T_mean / K'] == temperature,
                'frozen_fraction_unc_minus / -'].to_numpy()
            ff_unc_plus = data_corrected.loc[
                data_corrected['T_mean / K'] == temperature,
                'frozen_fraction_unc_plus / -'].to_numpy()

            new_cINP_sol = cINP_sol.mean()

            new_cINP_air, new_cINP_air_unc_minus, new_cINP_air_unc_plus = add_asym(
                cINP_air,
                cINP_air_unc_minus,
                cINP_air_unc_plus)
            new_ff, new_ff_unc_minus, new_ff_unc_plus = add_asym(ff,
                                                                 ff_unc_minus,
                                                                 ff_unc_plus)

            unc_propagation[temperature] = {'new_cINP_air': new_cINP_air,
                                            'new_cINP_air_unc_minus': new_cINP_air_unc_minus,
                                            'new_cINP_air_unc_plus': new_cINP_air_unc_plus,
                                            'new_ff': new_ff,
                                            'new_ff_unc_minus': new_ff_unc_minus,
                                            'new_ff_unc_plus': new_ff_unc_plus,
                                            'new_cINP_sol': new_cINP_sol}

        data_corrected.drop_duplicates(subset='T_mean / K', keep='first', inplace=True)
        for temperature in duplicate_temps:
            data_corrected.loc[data_corrected['T_mean / K'] == temperature,
                               'c_INP_unc_minus / 1/mg'] = unc_propagation[temperature][
                                   'new_cINP_air_unc_minus']
            data_corrected.loc[data_corrected['T_mean / K'] == temperature,
                               'c_INP_unc_plus / 1/mg'] = unc_propagation[temperature][
                                   'new_cINP_air_unc_plus']
            data_corrected.loc[data_corrected['T_mean / K'] == temperature,
                               'frozen_fraction_unc_minus / -'] = unc_propagation[temperature][
                                   'new_ff_unc_minus']
            data_corrected.loc[data_corrected['T_mean / K'] == temperature,
                               'frozen_fraction_unc_plus / -'] = unc_propagation[temperature][
                                   'new_ff_unc_minus']
            data_corrected.loc[data_corrected['T_mean / K'] == temperature,
                               'c_INP / 1/l'] = unc_propagation[temperature][
                                   'new_cINP_air']
            data_corrected.loc[data_corrected['T_mean / K'] == temperature,
                               'frozen_fraction / -'] = unc_propagation[temperature][
                                   'new_ff']
            data_corrected.loc[data_corrected['T_mean / K'] == temperature,
                               'c_INP_sol / 1/mg_sol'] = unc_propagation[temperature][
                                   'new_cINP_sol']

    else:
        data_corrected_old = data_corrected.copy(deep=True)
        df_lst = []
        for dilution in data_corrected_old.index.get_level_values(0).unique():
            maximum_index = data_corrected_old.loc[(dilution, slice(None)),
                                                   'c_INP / 1/l'].idxmax()
            df_dilution = data_corrected_old.loc[(dilution, slice(None)), :]
            df_dilution.index = df_dilution.index.get_level_values(1)
            df_dilution = df_dilution.loc[:maximum_index[1], :]
            df_dilution.drop_duplicates(subset='T_mean / K', keep='first', inplace=True)

            df_dilution['dilution'] = [dilution for _ in range(len(df_dilution.index))]
            df_dilution = df_dilution.set_index('dilution', append=True)
            df_dilution = df_dilution.reorder_levels(['dilution', None])
            df_lst.append(df_dilution)

        try:
            data_corrected = pd.concat(df_lst, join='outer')
        except ValueError as e:
            print(f'{e}.')
        # no uncertainty propagation if events are choosen
        if T_step is None:
            duplicate_temps = []
        else:
            duplicate_temps = data_corrected.loc[
                data_corrected.duplicated(subset='T_mean / K', keep=False), 'T_mean / K'].unique()

        unc_propagation = {}
        for temperature in duplicate_temps:
            cINP_air = data_corrected.loc[
                data_corrected['T_mean / K'] == temperature,
                'c_INP / 1/l'].to_numpy()
            cINP_air_unc_minus = data_corrected.loc[
                data_corrected['T_mean / K'] == temperature,
                'c_INP_unc_minus / 1/l'].to_numpy()
            cINP_air_unc_plus = data_corrected.loc[
                data_corrected['T_mean / K'] == temperature,
                'c_INP_unc_plus / 1/l'].to_numpy()

            cINP_sol = data_corrected.loc[
                data_corrected['T_mean / K'] == temperature,
                'c_INP_sol / 1/l_sol'].to_numpy()

            ff = data_corrected.loc[
                data_corrected['T_mean / K'] == temperature,
                'frozen_fraction / -'].to_numpy()
            ff_unc_minus = data_corrected.loc[
                data_corrected['T_mean / K'] == temperature,
                'frozen_fraction_unc_minus / -'].to_numpy()
            ff_unc_plus = data_corrected.loc[
                data_corrected['T_mean / K'] == temperature,
                'frozen_fraction_unc_plus / -'].to_numpy()

            new_cINP_sol = cINP_sol.mean()

            new_cINP_air, new_cINP_air_unc_minus, new_cINP_air_unc_plus = add_asym(
                cINP_air,
                cINP_air_unc_minus,
                cINP_air_unc_plus)
            new_ff, new_ff_unc_minus, new_ff_unc_plus = add_asym(ff,
                                                                 ff_unc_minus,
                                                                 ff_unc_plus)

            unc_propagation[temperature] = {'new_cINP_air': new_cINP_air,
                                            'new_cINP_air_unc_minus': new_cINP_air_unc_minus,
                                            'new_cINP_air_unc_plus': new_cINP_air_unc_plus,
                                            'new_ff': new_ff,
                                            'new_ff_unc_minus': new_ff_unc_minus,
                                            'new_ff_unc_plus': new_ff_unc_plus,
                                            'new_cINP_sol': new_cINP_sol}

        data_corrected.drop_duplicates(subset='T_mean / K', keep='first', inplace=True)
        for temperature in duplicate_temps:
            data_corrected.loc[data_corrected['T_mean / K'] == temperature,
                               'c_INP_unc_minus / 1/l'] = unc_propagation[temperature][
                                   'new_cINP_air_unc_minus']
            data_corrected.loc[data_corrected['T_mean / K'] == temperature,
                               'c_INP_unc_plus / 1/l'] = unc_propagation[temperature][
                                   'new_cINP_air_unc_plus']
            data_corrected.loc[data_corrected['T_mean / K'] == temperature,
                               'frozen_fraction_unc_minus / -'] = unc_propagation[temperature][
                                   'new_ff_unc_minus']
            data_corrected.loc[data_corrected['T_mean / K'] == temperature,
                               'frozen_fraction_unc_plus / -'] = unc_propagation[temperature][
                                   'new_ff_unc_minus']
            data_corrected.loc[data_corrected['T_mean / K'] == temperature,
                               'c_INP / 1/l'] = unc_propagation[temperature][
                                   'new_cINP_air']
            data_corrected.loc[data_corrected['T_mean / K'] == temperature,
                               'frozen_fraction / -'] = unc_propagation[temperature][
                                   'new_ff']
            data_corrected.loc[data_corrected['T_mean / K'] == temperature,
                               'c_INP_sol / 1/l_sol'] = unc_propagation[temperature][
                                   'new_cINP_sol']

    data_corrected.index = data_corrected.index.droplevel(level=0)
    print(f'Exp_id: {exp_id} - corrected data \n', data_corrected)

    pkl_path.joinpath(f"{exp_id}").mkdir(parents=True, exist_ok=True)
    if not T_step:
        T_step = 'event'
    if type(exp_id_blank) == int:
        data.to_pickle(pkl_path.joinpath(
            f'{exp_id}'
            f'/INSEKT_{exp_id}_'
            f'blank_{exp_id_blank}_{T_step}_Q{q_value}_T{t_calib}'
            '_uncorrected.pkl'))
        print(f'Created ./Pickles/{exp_id}/INSEKT_{exp_id}_blank_{exp_id_blank}'
              f'_{T_step}_Q{q_value}_T{t_calib}_uncorrected.pkl.')
        data_corrected.to_pickle(pkl_path.joinpath(
            f'{exp_id}'
            f'/INSEKT_{exp_id}_'
            f'blank_{exp_id_blank}_'
            f'{T_step}_Q{q_value}_T{t_calib}'
            '.pkl'))
        print(f'Created ./Pickles/{exp_id}/INSEKT_{exp_id}_blank_{exp_id_blank}'
              f'_{T_step}_Q{q_value}_T{t_calib}.pkl.')
    else:
        data.to_pickle(
            pkl_path.joinpath(
                f'{exp_id}/INSEKT_{exp_id}_{T_step}_Q{q_value}_T{t_calib}_uncorrected.pkl'))
        print(f'Created ./Pickles/{exp_id}/INSEKT_{exp_id}_{T_step}_'
              f'Q{q_value}_T{t_calib}_uncorrected.pkl.')
        data_corrected.to_pickle(pkl_path.joinpath(
            f'{exp_id}/INSEKT_{exp_id}_{T_step}_Q{q_value}_T{t_calib}.pkl'))
        print(f'Created ./Pickles/{exp_id}/INSEKT_{exp_id}_{T_step}_'
              f'Q{q_value}_T{t_calib}.pkl.')
    data_ff.to_pickle(pkl_path.joinpath(
        f'{exp_id}/INSEKT_{exp_id}_{T_step}'
        f'_frozen_fraction_T{t_calib}.pkl'))
    print(f'Created ./Pickles/{exp_id}/INSEKT_{exp_id}_{T_step}_frozen_fraction_T{t_calib}.pkl')
    data_dct = {'data': data,
                'data_corrected': data_corrected,
                'data_frozen_fraction': data_ff}
    return data_dct
