# -*- coding: utf-8 -*-
"""
Created on Tue Oct 13 10:59:12 2020

@author: uudyv

This file contains functions to select specific experiments from the INSEKT-Database:
    select_campaign    - Function that returns the campaign data from the exp_all table in the
                         INSEKT-Database.
    select_all_filters - Function that returns the data from the exp_all table in the INSEKT-
                         Database for all filter experiments.
    select_experiments - Function that returns the data from the exp_all table in the INSEKT-
                         Database for a given experiment (int) or list of experiments (list).
"""

from typing import TypeVar

PandasDataFrame = TypeVar('pandas.core.frame.DataFrame')


def check_credentials() -> tuple[str, str]:
    """
    Function to check for credentials for the insekt sql database.
    Originally written by Tobias Schorr.

    Returns
    -------
    tuple[str, str]
        Username and password for the insekt database.

    """
    import os
    import getpass
    from dotenv import load_dotenv, find_dotenv

    # a) first check if credentials are already stored in environment variables
    username = os.getenv('MARIADB_USERNAME')
    password = os.getenv('MARIADB_PASSWORD')

    # b) try to find credentials in .env file
    if not username and not password:
        load_dotenv(find_dotenv())
        username = os.getenv('MARIADB_USERNAME')
        password = os.getenv('MARIADB_PASSWORD')

    # c) ask for credentials and save in environment variables
    if not username and not password:
        username = input("To access the mariaDB database, "
                         "please enter your username:\n")
        os.environ['MARIADB_USERNAME'] = username
        password = getpass.getpass('\nTo access the mariaDB database, '
                                   'please enter your password:\n')
        os.environ['MARIADB_PASSWORD'] = password

    return username, password


def select_campaign(campaign: str, skip: list[int] | type(None) = None,
                    add: str | type(None) = None,
                    bulk: bool = False) -> PandasDataFrame:
    """
    Get exp_list data for a given campaign.

    Parameters
    ----------
    campaign : str
        Campaign name.
    skip : list[int] | type(None), optional
        Skip the type ids in the list. The default is None.
    add : str | type(None), optional
        Add additional specifiers to SQL query. The default is None.
    bulk : bool, optional
        Whether you just select filter or also bulk samples. The default is False.

    Returns
    -------
    PandasDataFrame
        Data from the exp_all table in the INSEKT-Database.

    """

    import numpy as np
    from colorama import Fore
    from colorama import Style
    import sqlalchemy as db
    import pandas as pd

    username, password = check_credentials()
    insekt = db.create_engine(f'mysql+mysqlconnector://{username}:{password}'
                              '@141.52.172.3:3306/insekt')

    if bulk:
        query = (f"SELECT * from insekt.exp_all WHERE campaign = '{campaign}'"
                 " AND (samplename IS NULL"
                 " OR samplename NOT LIKE '%blank%'"
                 " OR samplename NOT LIKE '%Blank%'"
                 " OR samplename NOT LIKE '%BLANK%')"
                 " AND (comment IS NULL"
                 " OR comment NOT LIKE '%blank%'"
                 " OR comment NOT LIKE '%Blank%'"
                 " OR comment NOT LIKE '%BLANK%')"
                 " AND (sampledescr IS NULL"
                 " OR sampledescr NOT LIKE '%blank%'"
                 " OR sampledescr NOT LIKE '%Blank%'"
                 " OR sampledescr NOT LIKE '%BLANK%')"
                 " AND (samplename IS NULL"
                 " OR samplename NOT LIKE '%blind%'"
                 " OR samplename NOT LIKE '%Blind%'"
                 " OR samplename NOT LIKE '%BLIND%')"
                 " AND (comment IS NULL"
                 " OR comment NOT LIKE '%blind%'"
                 " OR comment NOT LIKE '%Blind%'"
                 " OR comment NOT LIKE '%BLIND%')"
                 " AND (sampledescr IS NULL"
                 " OR sampledescr NOT LIKE '%blind%'"
                 " OR sampledescr NOT LIKE '%Blind%'"
                 " OR sampledescr NOT LIKE '%BLIND%')"
                 " AND mask_file LIKE '%.png%'"
                 " AND exp_id NOT IN (100, 101, 102, 103, 104, 105, 242, 243)"
                 " AND type = 'bulk'"
                 " AND (comment IS NULL OR comment NOT LIKE '%problem%')"
                 " AND ((t_stop - t_start) > 500)")
    else:
        query = (f"SELECT * from insekt.exp_all WHERE campaign = '{campaign}'"
                 " AND (samplename IS NULL"
                 " OR samplename NOT LIKE '%blank%'"
                 " OR samplename NOT LIKE '%Blank%'"
                 " OR samplename NOT LIKE '%BLANK%')"
                 " AND (comment IS NULL"
                 " OR comment NOT LIKE '%blank%'"
                 " OR comment NOT LIKE '%Blank%'"
                 " OR comment NOT LIKE '%BLANK%')"
                 " AND (sampledescr IS NULL"
                 " OR sampledescr NOT LIKE '%blank%'"
                 " OR sampledescr NOT LIKE '%Blank%'"
                 " OR sampledescr NOT LIKE '%BLANK%')"
                 " AND (samplename IS NULL"
                 " OR samplename NOT LIKE '%blind%'"
                 " OR samplename NOT LIKE '%Blind%'"
                 " OR samplename NOT LIKE '%BLIND%')"
                 " AND (comment IS NULL"
                 " OR comment NOT LIKE '%blind%'"
                 " OR comment NOT LIKE '%Blind%'"
                 " OR comment NOT LIKE '%BLIND%')"
                 " AND (sampledescr IS NULL"
                 " OR sampledescr NOT LIKE '%blind%'"
                 " OR sampledescr NOT LIKE '%Blind%'"
                 " OR sampledescr NOT LIKE '%BLIND%')"
                 " AND mask_file LIKE '%.png%'"
                 " AND exp_id NOT IN (100, 101, 102, 103, 104, 105, 242, 243)"
                 " AND type = 'filter'"
                 " AND (comment IS NULL OR comment NOT LIKE '%problem%')"
                 " AND ((t_stop - t_start) > 500)")

    if type(add) == str:
        query = query + ' ' + add

    print(f'This is your sql query currently: {Fore.CYAN}{Style.BRIGHT}{query}{Style.RESET_ALL}')

    data = pd.read_sql_query(query, insekt)
    data['data'] = data['data'].map(lambda x: np.delete(np.frombuffer(x[::-1],
                                                                      dtype=np.int8),
                                                        [-4, -3, -2, -1]))

    if skip:
        type_id = [x for x in data['type_id'] if x in skip]
        data = data[~data['type_id'].isin(type_id)]

    query_timeseries = ("SELECT * FROM insekt.larissa WHERE "
                        f"exp_id IN {tuple(data.exp_id.to_list())}")

    data_timeseries = pd.read_sql_query(query_timeseries, insekt)
    data = pd.merge(data, data_timeseries, on='exp_id')

    print(data.loc[:, 'exp_id':'samplename'])

    return data


def select_all_filters(skip: list | type(None) = None, blank: bool = False,
                       add: str | type(None) = None) -> PandasDataFrame:
    """
    Functions that collects data from the whole of INSEKT.

    Parameters
    ----------
    skip : list | type(None), optional
        Skip the type ids in the list. The default is None.
    blank : bool, optional
        Select blanks or select normal filters. The default is False.
    add : str | type(None), optional
        Add additional specifiers to SQL query. The default is None.

    Returns
    -------
    PandasDataFrame
        Data from the exp_all table in the INSEKT-Database.

    """

    import numpy as np
    import sqlalchemy as db
    import pandas as pd

    username, password = check_credentials()
    insekt = db.create_engine(f'mysql+mysqlconnector://{username}:{password}'
                              '@141.52.172.3:3306/insekt')

    query = ("SELECT * from insekt.exp_all WHERE type = 'filter'"
             " AND (samplename IS NULL"
             " OR samplename NOT LIKE '%blank%'"
             " OR samplename NOT LIKE '%Blank%'"
             " OR samplename NOT LIKE '%BLANK%')"
             " AND (comment IS NULL"
             " OR comment NOT LIKE '%blank%'"
             " OR comment NOT LIKE '%Blank%'"
             " OR comment NOT LIKE '%BLANK%')"
             " AND (sampledescr IS NULL"
             " OR sampledescr NOT LIKE '%blank%'"
             " OR sampledescr NOT LIKE '%Blank%'"
             " OR sampledescr NOT LIKE '%BLANK%')"
             " AND (samplename IS NULL"
             " OR samplename NOT LIKE '%blind%'"
             " OR samplename NOT LIKE '%Blind%'"
             " OR samplename NOT LIKE '%BLIND%')"
             " AND (comment IS NULL"
             " OR comment NOT LIKE '%blind%'"
             " OR comment NOT LIKE '%Blind%'"
             " OR comment NOT LIKE '%BLIND%')"
             " AND (sampledescr IS NULL"
             " OR sampledescr NOT LIKE '%blind%'"
             " OR sampledescr NOT LIKE '%Blind%'"
             " OR sampledescr NOT LIKE '%BLIND%')"
             " AND mask_file LIKE '%.png%'"
             " AND exp_id NOT IN (100, 101, 102, 103, 104, 105, 242, 243)"
             " AND (comment IS NULL OR comment NOT LIKE '%problem%')")

    if blank:
        query = ("SELECT * from insekt.exp_all"
                 " WHERE type != 'bulk'"
                 " AND"
                 " (samplename LIKE '%blank%'"
                 " OR samplename LIKE '%Blank%'"
                 " OR samplename LIKE '%BLANK%'"
                 " OR comment LIKE '%blank%'"
                 " OR comment LIKE '%Blank%'"
                 " OR comment LIKE '%BLANK%'"
                 " OR sampledescr LIKE '%blank%'"
                 " OR sampledescr LIKE '%Blank%'"
                 " OR sampledescr LIKE '%BLANK%'"
                 " OR samplename LIKE '%blind%'"
                 " OR samplename LIKE '%Blind%'"
                 " OR samplename LIKE '%BLIND%'"
                 " OR comment LIKE '%blind%'"
                 " OR comment LIKE '%Blind%'"
                 " OR comment LIKE '%BLIND%'"
                 " OR sampledescr LIKE '%blind%'"
                 " OR sampledescr LIKE '%Blind%'"
                 " OR sampledescr LIKE '%BLIND%')"
                 " AND mask_file IS NOT NULL"
                 " AND mask_file LIKE '%.png%'"
                 " AND exp_id NOT IN (100, 101, 102, 103, 104, 105, 242, 243)"
                 " AND (comment IS NULL OR comment NOT LIKE '%problem%')"
                 )

    if type(add) == str:
        query = query + ' ' + add

    data = pd.read_sql_query(query, insekt)
    data['data'] = data['data'].map(lambda x: np.delete(np.frombuffer(x[::-1],
                                                                      dtype=np.int8),
                                                        [-4, -3, -2, -1]))
    if skip is not None:
        type_id = [x for x in data['type_id'] if x in skip]
        data = data[~data['type_id'].isin(type_id)]

    query_timeseries = ("SELECT * FROM insekt.larissa WHERE "
                        f"exp_id IN {tuple(data.exp_id.to_list())}")

    data_timeseries = pd.read_sql_query(query_timeseries, insekt)
    data = pd.merge(data, data_timeseries, on='exp_id')

    print(data.loc[:, 'exp_id':'samplename'])

    return data


def select_all_bulk(skip: list | type(None) = None, blank: bool = False,
                    add: str | type(None) = None) -> PandasDataFrame:
    """
    Functions that collects data from the whole of INSEKT.

    Parameters
    ----------
    skip : list | type(None), optional
        Skip the type ids in the list. The default is None.
    blank : bool, optional
        Select blanks or select normal filters. The default is False.
    add : str | type(None), optional
        Add additional specifiers to SQL query. The default is None.

    Returns
    -------
    PandasDataFrame
        Data from the exp_all table in the INSEKT-Database..

    """

    import numpy as np
    import sqlalchemy as db
    import pandas as pd

    username, password = check_credentials()
    insekt = db.create_engine(f'mysql+mysqlconnector://{username}:{password}'
                              '@141.52.172.3:3306/insekt')

    query = ("SELECT * from insekt.exp_all WHERE type = 'bulk'"
             " AND (samplename IS NULL"
             " OR samplename NOT LIKE '%blank%'"
             " OR samplename NOT LIKE '%Blank%'"
             " OR samplename NOT LIKE '%BLANK%')"
             " AND (comment IS NULL"
             " OR comment NOT LIKE '%blank%'"
             " OR comment NOT LIKE '%Blank%'"
             " OR comment NOT LIKE '%BLANK%')"
             " AND (sampledescr IS NULL"
             " OR sampledescr NOT LIKE '%blank%'"
             " OR sampledescr NOT LIKE '%Blank%'"
             " OR sampledescr NOT LIKE '%BLANK%')"
             " AND (samplename IS NULL"
             " OR samplename NOT LIKE '%blind%'"
             " OR samplename NOT LIKE '%Blind%'"
             " OR samplename NOT LIKE '%BLIND%')"
             " AND (comment IS NULL"
             " OR comment NOT LIKE '%blind%'"
             " OR comment NOT LIKE '%Blind%'"
             " OR comment NOT LIKE '%BLIND%')"
             " AND (sampledescr IS NULL"
             " OR sampledescr NOT LIKE '%blind%'"
             " OR sampledescr NOT LIKE '%Blind%'"
             " OR sampledescr NOT LIKE '%BLIND%')"
             " AND mask_file LIKE '%.png%'"
             " AND exp_id NOT IN (100, 101, 102, 103, 104, 105, 242, 243)"
             " AND (comment IS NULL OR comment NOT LIKE '%problem%')")

    if blank:
        query = ("SELECT * from insekt.exp_all"
                 " WHERE type = 'bulk'"
                 " AND"
                 " (samplename LIKE '%blank%'"
                 " OR samplename LIKE '%Blank%'"
                 " OR samplename LIKE '%BLANK%'"
                 " OR comment LIKE '%blank%'"
                 " OR comment LIKE '%Blank%'"
                 " OR comment LIKE '%BLANK%'"
                 " OR sampledescr LIKE '%blank%'"
                 " OR sampledescr LIKE '%Blank%'"
                 " OR sampledescr LIKE '%BLANK%'"
                 " OR samplename LIKE '%blind%'"
                 " OR samplename LIKE '%Blind%'"
                 " OR samplename LIKE '%BLIND%'"
                 " OR comment LIKE '%blind%'"
                 " OR comment LIKE '%Blind%'"
                 " OR comment LIKE '%BLIND%'"
                 " OR sampledescr LIKE '%blind%'"
                 " OR sampledescr LIKE '%Blind%'"
                 " OR sampledescr LIKE '%BLIND%')"
                 " AND mask_file IS NOT NULL"
                 " AND mask_file LIKE '%.png%'"
                 " AND exp_id NOT IN (100, 101, 102, 103, 104, 105, 242, 243)"
                 " AND (comment IS NULL OR comment NOT LIKE '%problem%')"
                 )

    if type(add) == str:
        query = query + ' ' + add

    data = pd.read_sql_query(query, insekt)
    data['data'] = data['data'].map(lambda x: np.delete(np.frombuffer(x[::-1],
                                                                      dtype=np.int8),
                                                        [-4, -3, -2, -1]))
    if skip is not None:
        type_id = [x for x in data['type_id'] if x in skip]
        data = data[~data['type_id'].isin(type_id)]

    return data


def select_experiments(exp_id: list[int] | int) -> PandasDataFrame:
    """
    Select single experiments or a number of experiments.

    Parameters
    ----------
    exp_id : list[int] | int
        Experiment ids to select.

    Returns
    -------
    PandasDataFrame
        Data from the exp_all table in the INSEKT-Database.

    """
    import numpy as np
    import sqlalchemy as db
    import pandas as pd

    username, password = check_credentials()
    insekt = db.create_engine(f'mysql+mysqlconnector://{username}:{password}'
                              '@141.52.172.3:3306/insekt')

    if isinstance(exp_id, pd.Series):
        exp_id = exp_id.to_numpy()

    if isinstance(exp_id, (np.int64, int)):
        exp_id = np.array([exp_id])
    else:
        exp_id = np.asarray(exp_id)

    if len(exp_id) == 1:
        # new try and except statement to make sure native python type is used
        try:
            exp_id = exp_id[0].item()
        except AttributeError:
            exp_id = exp_id[0]
        query = f"SELECT * from insekt.exp_all WHERE exp_id = '{exp_id}'"
        query_timeseries = f"SELECT * FROM insekt.larissa WHERE exp_id = '{exp_id}'"
    else:
        try:
            exp_id = tuple([exp_id_value.item() for exp_id_value in exp_id])
        except AttributeError:
            exp_id = tuple(exp_id)
        query = f"SELECT * from insekt.exp_all WHERE exp_id IN {exp_id}"
        query_timeseries = f"SELECT * from insekt.larissa WHERE exp_id IN {exp_id}"

    data = pd.read_sql_query(query, insekt)
    data['data'] = data['data'].map(lambda x: np.delete(np.frombuffer(x[::-1],
                                                                      dtype=np.int8),
                                                        [-4, -3, -2, -1]))

    data_timeseries = pd.read_sql_query(query_timeseries, insekt)
    data = pd.merge(data, data_timeseries, on='exp_id')

    return data


def data_collector(exp_id: list[int] | int,
                   T_step: float | None = 0.5,
                   q_value: int = 8,
                   frozen_fraction_only: bool = False,
                   overwrite: bool = False) -> dict[int, dict[str, dict]]:
    """
    Helper function to combine various methods and collect data.
    This is the main function that should be used to analyze
    experiments from the INSEKT database.

    Parameters
    ----------
    exp_id : list[int] | int
        List of experiment ids or single
        experiment id of a given experiment.
    T_step : float | None, optional
        The temperature grid, where data points lie. The grid is on the Kelvin scale.
        If None is given, the data will be spread as events, i.e. whenever a well freezes,
        one data point is generated. The default is 0.5.
    q_value : int, optional
        q_value is a measure of the asymmetry in the uncertainties calculated
        with the approximated bernoulli PDF from Agresti & Coull. The default is 8.
    frozen_fraction_only : bool, optional
        For samples where you are only interested in the frozen fraction, or do not
        have the information of amount of air / amount of sample. The default is False.
    overwrite : bool, optional
        Data is saved inside the Pickle folder for each experiment. If you have already
        analysed data and just want to update for new analysis, reading the data and not
        overwriting is much faster. The default is False.

    Raises
    ------
    FileNotFoundError
        If overwrite = False, the function tries to read the data, if it is not found,
        it will be generated.

    Returns
    -------
    dict[int, dict[str, dict]]
        Returns a dictionary that contains the dict created by single_experiment().
        The keys are related to the experiment ids.

    """
    import numpy as np
    import pandas as pd
    from pathlib import Path
    from .INSEKT_analysis import single_experiment

    if isinstance(exp_id, int):
        exp_id_array = np.array([exp_id])
    else:
        exp_id_array = np.asarray(exp_id)

    pkl_path = Path(__file__).parent.joinpath('Pickles')

    if not T_step:
        T_step_name = 'event'
    else:
        T_step_name = T_step

    exp_lst = select_experiments(exp_id_array)

    analysis_data = {}

    for exp_id in exp_id_array:
        print('Exp_id: ', exp_id)
        if exp_lst.loc[exp_lst['exp_id'] == exp_id, 'insekt_id'].to_numpy()[0] == 1:
            if (exp_lst.loc[exp_lst['exp_id'] == exp_id,
                            'prep_time'].to_numpy()[0] < pd.to_datetime('2023-06-05 13:27:00')):
                calibration = 'Barbara_Kalibrierung_Kelvin'
            else:
                calibration = 'Jens_Kalibrierung_Kelvin'
        elif exp_lst.loc[exp_lst['exp_id'] == exp_id, 'insekt_id'].to_numpy()[0] == 2:
            if exp_lst.loc[exp_lst['exp_id'] == exp_id,
                           'prep_time'].to_numpy()[0] < pd.to_datetime('2023-06-05 13:27:00'):
                calibration = 'Calibration_in_Kelvin'
            else:
                calibration = 'Jens_Kalibrierung_Kelvin'
        try:
            if overwrite:
                raise FileNotFoundError
            if frozen_fraction_only:
                data_dct = {
                    'data': pd.DataFrame(),
                    'data_corrected': pd.DataFrame(),
                    'data_frozen_fraction': pd.read_pickle(pkl_path.joinpath(
                        f'{exp_id}/INSEKT_{exp_id}_{T_step_name}'
                        f'_frozen_fraction_T{calibration}.pkl'))}
            else:
                data_dct = {
                    'data': pd.read_pickle(pkl_path.joinpath(
                        f'{exp_id}/INSEKT_{exp_id}_{T_step_name}_'
                        f'Q{q_value}_T{calibration}_uncorrected.pkl')),
                    'data_corrected': pd.read_pickle(pkl_path.joinpath(
                        f'{exp_id}/INSEKT_{exp_id}_{T_step_name}_Q{q_value}_T{calibration}.pkl')),
                    'data_frozen_fraction': pd.read_pickle(pkl_path.joinpath(
                        f'{exp_id}/INSEKT_{exp_id}_{T_step_name}'
                        f'_frozen_fraction_T{calibration}.pkl'))}
            analysis_data[exp_id] = data_dct
        except FileNotFoundError as e:
            print(e)
            try:
                analysis_data[exp_id] = single_experiment(exp_id, T_step=T_step, q_value=q_value)
            except IndexError as e:
                print(e)
                print(f'Check exp_id {exp_id}.')
    return analysis_data
