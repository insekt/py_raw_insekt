This folder contains three python scripts that are run inside the py_insekt environment, created using the instruction from the py_raw_insekt code ([codebase](https://codebase.helmholtz.cloud/insekt/py_raw_insekt)). Please do not manipulate these files, but copy them to your respective folder, activate the environment and run the files yourself. If there are any issues, [Contact me](mailto:alexander.boehmlaender@kit.edu).

# Instructions
- Run analysis.py
- Run plotting.py
- Run flagging.py

# analysis.py
This script creates data from the raw data saved on SQL database. Data is saved in a combined way as a python pickle file. The code also creates human-readable csv files, which are split up into single files.

# plotting.py
This script contains some basic functions for plotting data from INSEKT, also contains some examples for different use cases.

# flagging.py
Some basic flags are used and written into a log file to make quality control for INSEKT easier. Work in progress.
