#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  4 11:05:12 2023

@author: alex

Example for generating INSEKT data for a given campaign.
Data is saved in total as pickle files, but can also be save as csv files.
"""

try:
    from py_raw_insekt import INSEKT_selection as sel
    from py_raw_insekt import INSEKT_analysis as ana
except ModuleNotFoundError:
    import sys
    # TODO: change this path to the software path where the py_raw_insekt folder is located
    sys.path.append('/home/alex/Dokumente/Python_Modules_GIT')
    from py_raw_insekt import INSEKT_selection as sel
    from py_raw_insekt import INSEKT_analysis as ana
import matplotlib.pyplot as plt
from pathlib import Path
import os
import pickle

os.chdir(Path(__file__).parent)

plt.rcParams.update(plt.rcParamsDefault)
plt.rcParams.update({'font.size': 22,
                     'interactive': True})  # TODO: change this for interactive plots

# TODO: change your campaign name here
campaign = 'PaCE22'
q_value = 8
bulk = False  # TODO: change this to False if you are looking at filter samples.
T_step = None  # TODO: change this to None if you want event plots.
if T_step:
    T_step_name = T_step
else:
    T_step_name = 'event'

# %% The following three lines can only be run when connected to the database at KIT.
exp_lst = sel.select_campaign(campaign, bulk=bulk)
# TODO: change overwrite depending on if you want to load data or create and overwrite data.
analysis_data = sel.data_collector(exp_lst['exp_id'], T_step=T_step, q_value=q_value,
                                   overwrite=True, frozen_fraction_only=False)

# %% Analyse single experiment in more detail for temperature sensors
# number is the exp_id for a given samplename
exp_id_to_be_checked = exp_lst.loc[exp_lst['samplename'] == 'PaCE22_UAV1_28092022',
                                   'exp_id'][1].item()

df, df_raw, (fig1, ax1, ax1_2), (fig2, ax2, ax2_2) = ana.raw_data_func(exp_id_to_be_checked,
                                                                       single_outlier=True,
                                                                       std=False,
                                                                       heating_period=False,
                                                                       plot=True)
fig1.show()
fig2.show()

df, df_raw = ana.raw_data_func(exp_id_to_be_checked,
                               single_outlier=False,
                               std=False,
                               heating_period=False,
                               plot=False)
fig, ax = plt.subplots()
for i in range(1, 8+1):
    ax.plot(df[f'T{i}'])
fig.show()

# %% Analyse single experiment in more detail for grey scale data
# number is the exp_id
grey_scale, number_wells, freeze_index, df = ana.grey_scale_data(exp_id_to_be_checked)

# %% Save data as pickle files.
with open(Path(__file__).parent.joinpath(f'exp_lst_{campaign}.pickle'), 'wb') as handle:
    pickle.dump(exp_lst, handle, protocol=pickle.HIGHEST_PROTOCOL)
with open(Path(__file__).parent.joinpath(f'analysis_data_{campaign}_{T_step_name}.pickle'),
          'wb') as handle:
    pickle.dump(analysis_data, handle, protocol=pickle.HIGHEST_PROTOCOL)

# %% Read pickle files. Run this if you are not at KIT!
with open(Path(__file__).parent.joinpath(f'exp_lst_{campaign}.pickle'), 'rb') as handle:
    exp_lst = pickle.load(handle)
with open(Path(__file__).parent.joinpath(f'analysis_data_{campaign}_{T_step_name}.pickle'),
          'rb') as handle:
    analysis_data = pickle.load(handle)

# %% Save data as csv files
data_path = Path(__file__).parent.joinpath(f'DATA_{campaign}')
data_path.mkdir(parents=True, exist_ok=True)

exp_lst.to_csv(data_path.joinpath(f'exp_lst_{campaign}.csv'))
for exp_id, exp_data in analysis_data.items():
    try:
        exp_data['data'].to_csv(data_path.joinpath(
            f'INSEKT_{exp_id}_{T_step}_Q{q_value}_corrected.csv'))
        exp_data['data_corrected'].to_csv(data_path.joinpath(
            f'INSEKT_{exp_id}_{T_step}_Q{q_value}_uncorrected.csv'))
        exp_data['data_frozen_fraction'].to_csv(data_path.joinpath(
            f'INSEKT_{exp_id}_{T_step}_frozen_fraction.csv'))
    except KeyError:
        exp_data['data_frozen_fraction'].to_csv(data_path.joinpath(
            f'INSEKT_{exp_id}_{T_step}_frozen_fraction.csv'))
