#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 19 16:32:35 2024

@author: alex
"""
import matplotlib.pyplot as plt
import pandas as pd
from pathlib import Path
import os
import pickle
import logging
import getpass
import sys
import datetime

now = datetime.datetime.now().strftime('%Y%m%d_%H%M')

os.chdir(Path(__file__).parent)

plt.rcParams.update(plt.rcParamsDefault)
plt.rcParams.update({'font.size': 22,
                     'interactive': True,  # TODO: change this for interactive plots
                     'figure.figsize': (16, 9),
                     'figure.constrained_layout.use': True})

# TODO: change your campaign name here
campaign = 'PaCE22'
q_value = 8
bulk = False  # TODO: change this to False if you are looking at filter samples.
T_step = None  # TODO: change this to None if you want event plots.
if T_step:
    T_step_name = T_step
else:
    T_step_name = 'event'

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
formatter = logging.Formatter(fmt="%(asctime)s,%(levelname)s,%(message)s",
                              datefmt="%Y-%m-%d %H:%M:%S")
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
ch.setFormatter(formatter)
fh = logging.FileHandler(f'flagging_{campaign}_{getpass.getuser()}_{now}.log', "w")
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)
log.addHandler(ch)
log.addHandler(fh)

# %% Read pickle files. Before running this, make sure to run analysis.py script!
with open(Path(__file__).parent.joinpath(f'exp_lst_{campaign}.pickle'), 'rb') as handle:
    exp_lst = pickle.load(handle)
    log.info(f'Loaded exp_lst_{campaign}.pickle')
with open(Path(__file__).parent.joinpath(f'analysis_data_{campaign}_{T_step_name}.pickle'),
          'rb') as handle:
    analysis_data = pickle.load(handle)
    log.info(f'Loaded analysis_data_{campaign}_{T_step_name}.pickle')

# %% Create dictionaries for frozen fraction curves
half_temps_campaign = {}
quarter_temps_campaign = {}
three_quarter_temps_campaign = {}
for exp_id in exp_lst['exp_id']:
    dataset = analysis_data[exp_id]
    frozen_fraction = dataset['data_frozen_fraction']

    samplename = exp_lst.loc[exp_lst['exp_id'] == exp_id, 'samplename'].to_numpy()[0]
    half_temps_campaign[exp_id] = {}
    quarter_temps_campaign[exp_id] = {}
    three_quarter_temps_campaign[exp_id] = {}

    for dil, dil_df in frozen_fraction.groupby(level=0):
        half_point = abs(dil_df['frozen_fraction / -'] - 0.5).idxmin()
        half_temp = (dil_df.loc[half_point, 'T_mean / K'], dil_df.loc[half_point, 'T_std / K'])
        half_temps_campaign[exp_id][dil] = half_temp

        quarter_point = abs(dil_df['frozen_fraction / -'] - 0.25).idxmin()
        quarter_temp = (dil_df.loc[quarter_point, 'T_mean / K'],
                        dil_df.loc[quarter_point, 'T_std / K'])
        quarter_temps_campaign[exp_id][dil] = quarter_temp

        three_quarter_point = abs(dil_df['frozen_fraction / -'] - 0.75).idxmin()
        three_quarter_temp = (dil_df.loc[three_quarter_point, 'T_mean / K'],
                              dil_df.loc[three_quarter_point, 'T_std / K'])
        three_quarter_temps_campaign[exp_id][dil] = three_quarter_temp

# %% Quality check for data, example for a short campaign (Pallas2023)
Path(f'PLOTS_{campaign}_quality').mkdir(exist_ok=True, parents=True)

flags_exp_id = []

fig, ax = plt.subplots(3, 1, sharex=True)
for i, (exp_id, dct) in enumerate(half_temps_campaign.items()):
    df_half = pd.DataFrame.from_dict(dct, orient='index')
    df_quarter = pd.DataFrame.from_dict(quarter_temps_campaign[exp_id], orient='index')
    df_three_quarter = pd.DataFrame.from_dict(three_quarter_temps_campaign[exp_id], orient='index')

    # quality of water
    if ((df_half.loc[:, 0] - df_half.loc['Dilution 0', 0]) < 1).iloc[1:].any():
        log.error(f'Water might be too close (0.5) for exp_id: {exp_id}!')
        flags_exp_id.append(exp_id)
    if ((df_quarter.loc[:, 0] - df_quarter.loc['Dilution 0', 0]) < 1).iloc[1:].any():
        log.error(f'Water might be too close (0.25) for exp_id: {exp_id}!')
    if ((df_three_quarter.loc[:, 0] - df_three_quarter.loc['Dilution 0', 0]) < 1).iloc[1:].any():
        log.error(f'Water might be too close (0.75) for exp_id: {exp_id}!')

    # quality of dilutions --> separation
    if (abs(df_half.loc['Dilution 1':, 0].diff()) < 1).any():
        log.warning(f'Dilutions might be too close (0.5) for exp_id: {exp_id}!')
        flags_exp_id.append(exp_id)
    if (abs(df_quarter.loc['Dilution 1':, 0].diff()) < 1).any():
        log.warning(f'Dilutions might be too close (0.25) for exp_id: {exp_id}!')
    if (abs(df_three_quarter.loc['Dilution 1':, 0].diff()) < 1).any():
        log.warning(f'Dilutions might be too close (0.75) for exp_id: {exp_id}!')

    # quality of dilutions --> freezing order
    if not df_half.loc['Dilution 1':, 0].is_monotonic_decreasing:
        log.error(f'Dilutions do not freeze in the correct order (0.5) for exp_id: {exp_id}!')
        flags_exp_id.append(exp_id)
    if not df_quarter.loc['Dilution 1':, 0].is_monotonic_decreasing:
        log.error(f'Dilutions do not freeze in the correct order (0.25) for exp_id: {exp_id}!')
    if not df_three_quarter.loc['Dilution 1':, 0].is_monotonic_decreasing:
        log.error(f'Dilutions do not freeze in the correct order (0.75) for exp_id: {exp_id}!')

    for idx in df_half.index:
        ax[0].errorbar(f'{exp_id}', df_half.loc[idx, 0],
                       yerr=df_half.loc[idx, 1],
                       marker='x', capsize=5, color=f'C{idx[-1]}')
    for idx in df_quarter.index:
        ax[1].errorbar(f'{exp_id}', df_quarter.loc[idx, 0],
                       yerr=df_quarter.loc[idx, 1],
                       marker='x', capsize=5, color=f'C{idx[-1]}')
    for idx in df_three_quarter.index:
        ax[2].errorbar(f'{exp_id}', df_three_quarter.loc[idx, 0],
                       yerr=df_three_quarter.loc[idx, 1],
                       marker='x', capsize=5, color=f'C{idx[-1]}')
    ax[2].tick_params(axis='x', labelrotation=90)
for j, ticklabel in enumerate(ax[2].get_xticklabels()):
    if int(ticklabel.get_text()) in flags_exp_id:
        ax[2].get_xticklabels()[j].set_color('r')
ax[0].set_ylabel(r'$T^\mathrm{0.5}$ / K')
ax[1].set_ylabel(r'$T^\mathrm{0.25}$ / K')
ax[2].set_ylabel(r'$T^\mathrm{0.75}$ / K')
for axis in ax:
    axis.grid()
fig.savefig(f'PLOTS_{campaign}_quality/INSEKT_frozen_fraction_quality.png')
plt.close('all')

logging.shutdown()
