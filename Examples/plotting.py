#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 10 15:53:00 2024

@author: alex
"""

import matplotlib as mpl
import matplotlib.colors as mcolors
import matplotlib.pyplot as plt
import pandas as pd
from pathlib import Path
import os
import pickle
from scipy.stats import linregress, kstest
import numpy as np
from itertools import permutations

os.chdir(Path(__file__).parent)

plt.rcParams.update(plt.rcParamsDefault)
plt.rcParams.update({'font.size': 22,
                     'interactive': False,  # TODO: change this for interactive plots
                     'figure.figsize': (16, 9),
                     'figure.constrained_layout.use': True})

# TODO: change your campaign name here
campaign = 'PaCE22'
q_value = 8
bulk = True  # TODO: change this to False if you are looking at filter samples.
T_step = None  # TODO: change this to None if you want event plots.
if T_step:
    T_step_name = T_step
else:
    T_step_name = 'event'

# %% Read pickle files. Before running this, make sure to run analysis.py script!
with open(Path(__file__).parent.joinpath(f'exp_lst_{campaign}.pickle'), 'rb') as handle:
    exp_lst = pickle.load(handle)
with open(Path(__file__).parent.joinpath(f'analysis_data_{campaign}_{T_step_name}.pickle'),
          'rb') as handle:
    analysis_data = pickle.load(handle)

# %% Plotting only frozen fraction of campaign dataset
Path(f'PLOTS_{campaign}').mkdir(exist_ok=True, parents=True)

half_temps_campaign = {}
quarter_temps_campaign = {}
three_quarter_temps_campaign = {}
for exp_id in exp_lst['exp_id']:
    dataset = analysis_data[exp_id]
    frozen_fraction = dataset['data_frozen_fraction']

    samplename = exp_lst.loc[exp_lst['exp_id'] == exp_id, 'samplename'].to_numpy()[0]
    half_temps_campaign[exp_id] = {}
    quarter_temps_campaign[exp_id] = {}
    three_quarter_temps_campaign[exp_id] = {}

    fig, ax = plt.subplots()
    for dil, dil_df in frozen_fraction.groupby(level=0):
        half_point = abs(dil_df['frozen_fraction / -'] - 0.5).idxmin()
        half_temp = (dil_df.loc[half_point, 'T_mean / K'], dil_df.loc[half_point, 'T_std / K'])
        half_temps_campaign[exp_id][dil] = half_temp

        quarter_point = abs(dil_df['frozen_fraction / -'] - 0.25).idxmin()
        quarter_temp = (dil_df.loc[quarter_point, 'T_mean / K'],
                        dil_df.loc[quarter_point, 'T_std / K'])
        quarter_temps_campaign[exp_id][dil] = quarter_temp

        three_quarter_point = abs(dil_df['frozen_fraction / -'] - 0.75).idxmin()
        three_quarter_temp = (dil_df.loc[three_quarter_point, 'T_mean / K'],
                              dil_df.loc[three_quarter_point, 'T_std / K'])
        three_quarter_temps_campaign[exp_id][dil] = three_quarter_temp

        if dil in 'Dilution 0':
            dil = 'Nanopure'
        ax.errorbar(dil_df['T_mean / K'], dil_df['frozen_fraction / -'],
                    xerr=dil_df['T_std / K'],
                    yerr=[dil_df['frozen_fraction_unc_minus / -'],
                          dil_df['frozen_fraction_unc_plus / -']],
                    marker='x', linestyle='', capsize=5, label=f'{dil} ({half_temp[0]:.2f} K)')
    ax_sec = ax.secondary_xaxis('top', functions=(lambda x: x-273.15, lambda x: x-273.15))
    ax_sec.set_xlabel('$T$ / °C')
    ax.grid()
    ax.legend(loc='upper right')
    ax.set_ylabel('frozen fraction / -')
    ax.set_xlabel('$T$ / K')
    ax.set_title(samplename, pad=10)
    fig.savefig(f'PLOTS_{campaign}/INSEKT_frozen_fraction_{samplename}'
                f'_{exp_id}_q{q_value}_T{T_step_name}.png')
    plt.close(fig)


# %% Plotting just water, example for test campaign.
Path(f'PLOTS_{campaign}_quality').mkdir(exist_ok=True, parents=True)

cmap = plt.get_cmap('viridis')
i = 0
fig, ax = plt.subplots()
for exp_id in exp_lst['exp_id']:
    # delete this for other campaigns and check which partitions you are interested in
    # this is needed for example if you have two exp_ids for one INSEKT run, where
    # typically the same water is used for both exp_ids.

    dataset = analysis_data[exp_id]
    frozen_fraction = dataset['data_frozen_fraction']

    samplename = exp_lst.loc[exp_lst['exp_id'] == exp_id, 'samplename'].to_numpy()[0]

    insekt_id = exp_lst.loc[exp_lst['exp_id'] == exp_id, 'insekt_id'].to_numpy()[0]
    prep_time = (exp_lst.loc[exp_lst['exp_id'] == exp_id, 'prep_time']
                 .to_numpy()[0])
    prep_time = pd.to_datetime(prep_time)

    for dil, dil_df in frozen_fraction.groupby(level=0):
        if dil in 'Dilution 0':
            dil = 'Nanopure'
            half_point = abs(dil_df['frozen_fraction / -'] - 0.5).idxmin()
            half_temp = (dil_df.loc[half_point, 'T_mean / K'], dil_df.loc[half_point, 'T_std / K'])
            ax.errorbar(dil_df['T_mean / K'], dil_df['frozen_fraction / -'],
                        xerr=dil_df['T_std / K'],
                        yerr=[dil_df['frozen_fraction_unc_minus / -'],
                              dil_df['frozen_fraction_unc_plus / -']],
                        marker='x', linestyle='', capsize=5,
                        color=cmap(i / 36),
                        label=(f'{samplename} ({half_temp[0]:.2f} K)'
                               '\n'
                               f' {prep_time}, {insekt_id}'))
ax.axvline(273.15 - 25, color='k', label='"good" water line')
ax_sec = ax.secondary_xaxis('top', functions=(lambda x: x-273.15, lambda x: x-273.15))
ax_sec.set_xlabel('$T$ / °C')
ax.grid()
ax.legend(loc='upper right', ncols=4, fontsize=5)
ax.set_ylabel('frozen fraction / -')
ax.set_xlabel('$T$ / K')
ax.set_xlim(243, ax.get_xlim()[-1])
ax.set_title(campaign, pad=10)
fig.savefig(f'PLOTS_{campaign}_quality/INSEKT_frozen_fraction_{campaign}_water.png',
            dpi=600)
plt.close('all')

# %%
norm = mcolors.Normalize(vmin=6, vmax=20)
cmap = plt.get_cmap('viridis')

i = 0
prep_time_dict = {}

fig, ax = plt.subplots()
for exp_id in exp_lst['exp_id']:
    # delete this for other campaigns and check which partitions you are interested in
    # this is needed for example if you have two exp_ids for one INSEKT run, where
    # typically the same water is used for both exp_ids.

    dataset = analysis_data[exp_id]
    frozen_fraction = dataset['data_frozen_fraction']

    samplename = exp_lst.loc[exp_lst['exp_id'] == exp_id, 'samplename'].to_numpy()[0]

    insekt_id = exp_lst.loc[exp_lst['exp_id'] == exp_id, 'insekt_id'].to_numpy()[0]
    prep_time = (exp_lst.loc[exp_lst['exp_id'] == exp_id, 'prep_time']
                 .to_numpy()[0])
    prep_time = pd.to_datetime(prep_time)
    half_temp = half_temps_campaign[exp_id]['Dilution 0']
    prep_time_dict[exp_id] = (prep_time, half_temp[0])
    ax.errorbar(prep_time, half_temp[0], yerr=half_temp[1],
                marker='x', linestyle='', capsize=5,
                color=cmap(prep_time.hour / (norm.vmax - norm.vmin)),
                label=(f'{samplename} ({half_temp[0]:.2f} K)'
                       '\n'
                       f' {prep_time}, {insekt_id}'))
fig.colorbar(mpl.cm.ScalarMappable(norm=norm, cmap=cmap), ax=ax, label='hour of day / -')
ax.axhline(273.15 - 25, color='k', label='"good" water line')
ax.grid()
# ax.legend(loc='upper right', ncols=3, fontsize=5)
ax.set_ylabel('frozen fraction / -')
ax.set_xlabel('$T$ / K')
ax.set_title(campaign, pad=10)
fig.savefig(f'PLOTS_{campaign}_quality/INSEKT_frozen_fraction_{campaign}_water.png',
            dpi=600)
