# -*- coding: utf-8 -*-
"""
Created on Tue Oct 13 10:54:09 2020

@author: uudyv

This file contains functions to calculate different parameters related to the INSEKT experiment:
    c_inp_np_corr - Function to calculate the INP concentration per litre of air, corrected by
                    nanopure water.
    c_inp_sol     - Function to calculate the INP concentration per litre of solution, corrected
                    by nanopure water.
    c_inp_unc     - Function to calculate the uncertainty of the INP concentration per litre of
                    air. Determined by the Wilson interval (see e.g. Agresti & Coull 1998).
"""

from numpy.typing import NDArray


def c_inp_np_corr(V_sol: float, V_air: float, sample_time: float, d: int, f_lw: NDArray[float],
                  f_l: NDArray[float]) -> NDArray[float]:
    """

    Parameters
    ----------
    V_sol : float
        Solution volume in milliliter (e.g. 8 for 8 ml).
    V_air : float
        Sample flow in liter per minute (e.g. 10 lpm).
    sample_time : float
        Sample time in minutes, taken from sql_database insekt.larissa.
    d : int
        Scale factor, dependent on dilution (e.g. 1, 5, 25).
    V_well : float
        Volume of each PCR plate well. Fixed to 50 ul, i.e. 50e-6 l.
    f_lw : NDArray[float]
        Liquid fraction of nanopure water, calculated via greyscale raw data.
    f_l : NDArray[float]
        Liquid fraction of solution, calculated via greyscale raw data.

    Returns
    -------
    c_inp_np : NDArray[float]
        INP concentration with water correction in units of liter to the minus one (l^-1),
        according to Vali (1971).

    """
    import numpy as np

    V_well = 50e-6
    try:
        c_inp_np = (V_sol * 1e-3 / (V_air * sample_time)) * (d / V_well) * np.log((f_lw / f_l))
    except TypeError:
        print('There is a TypeError, check your variables:',
              f'V_sol = {type(V_sol)}',
              f'V_air = {type(V_air)}',
              f'sample_time = {type(sample_time)}',
              f'd = {type(d)}',
              f'V_well = {type(V_well)}',
              f'f_lw = {type(f_lw)}',
              f'f_l = {type(f_l)}',
              sep='\n')
    return c_inp_np


def c_inp_sol(dil_scale: int, f_l: NDArray[float], f_lw: NDArray[float]) -> NDArray[float]:
    """

    Parameters
    ----------
    dil_scale : int
        Dilution scale for the given aerosol suspension, taken from database.
    f_l : NDArray[float]
        Liquid fraction of the given aerosol suspension, temperature dependent.
    f_lw : NDArray[float]
        Liquid fraction of the nanopure water, temperature dependent.

    Returns
    -------
    c_inp : float
        INP concentration in (1/l_suspension).

    """
    import numpy as np

    V_well = 50  # 50 ul
    c_inp = - dil_scale / V_well * np.log(f_l / f_lw)
    return c_inp


def c_inp_unc(frozen_fraction: NDArray[float], num_wells: int, dil_scale: int) -> dict[str, float]:
    r"""
    Calculation of uncertainty using the Wilson interval, see also:
    Master thesis of Julia Kaufmann "Long-term Measurements of Ice Nucleating
                                     Particles in a Boreal Forest during the
                                     Winter to Spring Transition"
    PhD thesis of Thea Schiebel "Ice Nucleation Activity of Soil Dust Aerosols"
    Formulas taken from Thea Schiebel excel table for Cyprus 2017:
        \\imkaaf-srv1.imk-aaf.kit.edu\MessPC\INSEKT\Campaigns\201704_Cyprus\
            Cyprus2017_all-INSEKT.xlsx

    Parameters
    ----------
    frozen_fraction : NDArray[float]
        Frozen fraction of the given suspension.
    num_wells : int
        Total number of wells for given suspension.
    dil_scale : int
        Dilution scale for the given aerosol suspension, taken from database.

    Returns
    -------
    dict
        List of lower and upper confidence intervals. Careful, this is not already the final
        uncertainty. The final uncertainty depends on the INP concentration.

    """
    import numpy as np

    z = 1.96  # for 95 % confidence intervall
    V_well = 50  # 50 ul
    binomial_upper = (1 / (1 + z**2 / num_wells)
                      * (frozen_fraction + z**2 / (2 * num_wells)
                         + z * np.sqrt(frozen_fraction * (1 - frozen_fraction) / num_wells
                                       + z**2 / (4 * num_wells**2))))
    binomial_lower = (1 / (1 + z**2 / num_wells)
                      * (frozen_fraction + z**2 / (2 * num_wells)
                         - z * np.sqrt(frozen_fraction * (1 - frozen_fraction) / num_wells
                                       + z**2 / (4 * num_wells**2))))

    upper_limit_wells = binomial_upper * num_wells / num_wells
    lower_limit_wells = binomial_lower * num_wells / num_wells

    upper_limit_IN = - (np.log((num_wells - upper_limit_wells * num_wells)
                               / num_wells)
                        / V_well * dil_scale)
    lower_limit_IN = - (np.log((num_wells - lower_limit_wells * num_wells)
                               / num_wells)
                        / V_well * dil_scale)
    return {'lower_limit_wells': lower_limit_wells,
            'upper_limit_wells': upper_limit_wells,
            'lower_limit_IN': lower_limit_IN,
            'upper_limit_IN': upper_limit_IN}
