# py_raw_INSEKT

A collection of Python scripts that calculate INP concentrations from the INSEKT experiment and visualizes them in different ways.

Check calculations.pdf for details about calculation and code.

---

## Installation

### Clone the repository

Cloning and using git is the recommended way. This way you will have an easy time updating and keeping tracks of updates. Other ways might be supported but are not detailed in this readme.

Inside a git console, run the following command
```shell
git clone https://codebase.helmholtz.cloud/insekt/py_raw_insekt.git
```
Refer to any git workflow procedure afterwards.

### Setup of a new virtual environment

Since Python3.5 venv is the recommended way to create virtual environments ([documentation](https://docs.python.org/3/library/venv.html)). Invoking a new virtual environment is done by running

```shell
python -m venv /path/to/new/environment
```

This command will create a new folder at the specified path (i.e. a new folder `environment` is created in the folder `path/to/new`). This can be activated by running

```shell
source /path/to/new/enviroment/bin/activate
```

Refer to the [documentation](https://docs.python.org/3/library/venv.html) for the appropriate `activate` script for your system. After activating a number of Python modules need to be installed. This can be done automatically using the `requirements.txt` file provided or manually. If you choose to install packages via `pip` manually, there is no guarantee that everything will work correctly.

#### (a) Installation via requirements.txt (recommended)
Run the command

```shell
pip install --upgrade pip
pip install -r requirements.txt
```

Your new environment should be set up correctly and you can already open an editor, i.e. Spyder, to run the scripts provided in the [Examples](./Examples) folder.

#### (b) Manual installation (not recommended)

```shell
pip install --upgrade pip
pip install pandas matplotlib sqlalchemy mysql-connector-python python-dotenv colorama scipy spyder
```

The first line updates the `pip` package manager. This is recommended, but in general not always needed. In addition, `spyder` is used for editing the Python files, but any editor will work, so spyder can be removed from the list of requirements.

Afterwards you can run the codes provided in the [Examples](./Examples) folder.

---

## Contribute

**Any contribution to this project is welcome!**

The file `CONTRIBUTING.md` provides:
* A basic workflow guildeline for contributions.
* A list of all contributing persons.
* Further acknowledgements.

---

## License
Copyright (C) 2020-2024 KIT/IMK-AAF
(Karlsruhe Institute of Technology, Institute of Meteorology and
Climate Research, Atmospheric Aerosol Research)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the GNU General Public License can be found in the LICENSE file that
comes along with this program, or see <http://www.gnu.org/licenses/>.




